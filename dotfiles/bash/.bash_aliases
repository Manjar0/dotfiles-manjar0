#Extra bash aliases go here
if [ -x /bin/autojump ]; then
    alias aj='autojump'
fi

alias cl='clear'

if [ -x /usr/bin/colordiff ]; then
    alias diff='colordiff'
fi

alias hs='history'

# Pretty formatting for mount
alias mounttab='mount |column -t'

# Handy netstat aliases
alias nports='netstat -tulan'
alias nlisten='netstat -tulpe'

# Display all rules wwith iptables
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'

# Get web server headers
alias header='curl -I'
# Find out if remote server supports gzip / mod_deflate or not
alias headerc='curl -I --compress'

# Parenting changing perms on /
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# Distro specifc RHEL/CentOS update
alias update='yum update'
alias updatey='yum -y update'

# Control web servers via sudo
alias nginxreload='nginx -s reload'
alias nginxtest='nginx -t'
alias lightyload='lighttpd reload'
alias lightytest='lighttpd -f /etc/lighttpd/lighttpd.conf -t'
alias httpdreload='apachectl -k graceful'
alias httpdtest='apachectl -t && apachectl -t -D DUMP_VHOSTS'

# Pass options to free
alias meminfo='free -m -l -t'
# Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
# Get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

alias nocomment='grep -Ev "^#|^\ .*#"'

# Grabs the disk usage in the current directory
alias usage='du -ch | grep total'

# Gets the total disk usage on your machine
alias totalusage='df -hl --total | grep total'

# Shows the individual partition usages without the temporary memory values
alias partusage='df -hlT --exclude-type=tmpfs --exclude-type=devtmpfs'

# Gives you what is using the most space.
# Both directories and files. Varies on current dir.
alias most='du -hsx * | sort -rh | head -10'

# Set up a simple python webserver
alias pyserve='python3 -m http.server'

alias fsflush='sync'

# Get week number
alias week='date +%V'

# Validate json file with python
alias jsonlint='python -m json.tool'

#Openssl aliases, taken from:
#https://www.sslshopper.com/article-most-common-openssl-commands.html

#Check a Certificate Signing Request (CSR)
alias sslcsrcheck='openssl req -text -noout -verify -in'
#Check a private key
alias sslkeycheck='openssl rsa -check -in'
#Check a certificate
alias sslcertcheck='openssl x509 -text -noout -in'
#Check a PKCS#12 file (.pfx or .p12)
alias sslpkcscheck='openssl pkcs12 -info -in'
#Check an SSL connection. (hostname:port)
alias sslconncheck='openssl s_client -connect'

