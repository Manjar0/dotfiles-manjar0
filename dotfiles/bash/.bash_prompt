#!/usr/bin/env bash

# Shell prompt based on the Solarized Dark theme.
# Screenshot: http://i.imgur.com/EkEtphC.png
# Heavily inspired by @necolas’s prompt: https://github.com/necolas/dotfiles
# iTerm → Profiles → Text → use 13pt Monaco with 1.1 vertical spacing.

if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
    export TERM='gnome-256color';
elif infocmp xterm-256color >/dev/null 2>&1; then
    export TERM='xterm-256color';
fi;

prompt_git() {
    local s='';
    local branchName='';

    # Check if the current directory is in a Git repository.
    if [ $(git rev-parse --is-inside-work-tree &>/dev/null; echo "${?}") == '0' ]; then

        # check if the current directory is in .git before running git checks
        if [ "$(git rev-parse --is-inside-git-dir 2> /dev/null)" == 'false' ]; then

            # Ensure the index is up to date.
            git update-index --really-refresh -q &>/dev/null;

            # Check for uncommitted changes in the index.
            if ! $(git diff --quiet --ignore-submodules --cached); then
                s+='+';
            fi;

            # Check for unstaged changes.
            if ! $(git diff-files --quiet --ignore-submodules --); then
                s+='!';
            fi;

            # Check for untracked files.
            if [ -n "$(git ls-files --others --exclude-standard)" ]; then
                s+='?';
            fi;

            # Check for stashed files.
            if $(git rev-parse --verify refs/stash &>/dev/null); then
                s+='$';
            fi;

        fi;

        # Get the short symbolic ref.
        # If HEAD isn’t a symbolic ref, get the short SHA for the latest commit
        # Otherwise, just give up.
        branchName="$(git symbolic-ref --quiet --short HEAD 2> /dev/null || \
            git rev-parse --short HEAD 2> /dev/null || \
            echo '(unknown)')";

        [ -n "${s}" ] && s="${s}";
        if [ ${branchName} == 'master' ]; then
            echo -e "${1}M${2}${s}";
        else
            echo -e "${1}${branchName}${2}${s}";
        fi
    else
        return;
    fi;
}

# Prompt for openshift or kubernetes CLI
prompt_oc () {
    # Get current context
    CONTEXT=$(grep -Eo '^current-context: [^/]*' ~/.kube/config 2>/dev/null | cut -d ' ' -f 2)
    OCPRFX="k8s"
    if [[ -n "$CONTEXT" ]]; then
        OCCTXT="(${OCPRFX}:${CONTEXT})"
    fi
    echo -e "${1}${OCCTXT}${2}"
}

init_prompt () {
    if [[ $(grep -Fo 'token: ' ~/.kube/config 2> /dev/null) ]] && [[ $(oc whoami 2> /dev/null) ]]; then
        prompt_oc ${@}
    else
        prompt_git ${@}
    fi
}

if tput setaf 1 &> /dev/null; then
    tput sgr0; # reset colors
    bold=$(tput bold);
    reset=$(tput sgr0);
    # Solarized colors, taken from http://git.io/solarized-colors.
    black=$(tput setaf 0);
    blue=$(tput setaf 33);
    altblue=$(tput setaf 283);
    lightblue=$(tput setaf 4);
    cyan=$(tput setaf 37);
    altgreen=$(tput setaf 112);
    altgreen2=$(tput setaf 113);
    altgreen3=$(tput setaf 114);
    green=$(tput setaf 64);
    orange=$(tput setaf 166);
    purple=$(tput setaf 125);
    red=$(tput setaf 124);
    violet=$(tput setaf 61);
    white=$(tput setaf 15);
    yellow=$(tput setaf 136);
else
    bold='';
    reset="\e[0m";
    black="\e[1;30m";
    blue="\e[1;34m";
    altblue="${blue}";
    lightblue=${blue};
    cyan="\e[1;36m";
    altgreen= "2m";
    altgreen2="\e[01;32m";
    altgreen3="\e[01;32m";
    green="\e[1;32m";
    altgreen="${green}";
    altgreen2="${green}";
    altgreen3="${green}";
    orange="\e[1;33m";
    purple="\e[1;35m";
    red="\e[1;31m";
    violet="\e[1;35m";
    white="\e[1;37m";
    yellow="\e[1;33m";
fi;

# Highlight the user name when logged in as root.
if [[ "${USER}" == "root" ]]; then
    userStyle="${red}";
else
    userStyle="${altgreen}";
fi;

# Highlight the hostname when connected via SSH.
if [[ "${SSH_TTY}" ]]; then
    hostStyle="${bold}${altgreen3}";
else
    hostStyle="${altgreen3}";
fi;

# Set the terminal title and prompt.
PS1="\[\033]0;\W\007\]"; # working directory base name
#PS1+="\[${bold}\]\n"; # newline
PS1+="\[${userStyle}\]\u"; # username
PS1+="\[${altgreen2}\]@";
PS1+="\[${hostStyle}\]\h"; # host
PS1+="\[${white}\] ";
PS1+="\[${bold}${lightblue}\]\w"; # working directory full path
PS1+="\$(init_prompt \"\[${white}\]:\[${altblue}\]\" \"\[${blue}\]\")"; # Set oc or git prompt
PS1+=" ";
if [[ ${USER} == 'root' ]]; then
    PS1+="\[${white}\]# \[${reset}\]"; # `#` (and reset color)
else
    PS1+="\[${white}\]\$ \[${reset}\]"; # `$` (and reset color)
fi
export PS1;

PS2="\[${yellow}\]→ \[${reset}\]";
export PS2;

