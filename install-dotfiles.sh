#!/bin/sh

OWNER=$USER
INSTALL_DIR="/opt/dotfiles"
DOTFILES_SOURCE=`find $(dirname ${0%/*}) -maxdepth 1 -type d -name 'dotfiles'`
DOTFILES_BIN=`find $(dirname ${0%/*}) -maxdepth 1 -type f -name 'dotfiles.sh'`

if [[ -z ${DOTFILES_SOURCE} ]]; then
    echo "Cannot find dotfiles dir in current working directory, exiting..."
    exit 1
fi

if [[ -z ${DOTFILES_BIN} ]]; then
    echo "Cannot find dotfiles.sh script in current working directory, exiting..."
    exit 1
fi

if [[ ! -d ${INSTALL_DIR} ]]; then
    sudo mkdir -p ${INSTALL_DIR}
    sudo chown -R ${OWNER}. ${INSTALL_DIR}
fi

if [[ -d ${DOTFILES_SOURCE} ]]; then
    cp -ru ${DOTFILES_SOURCE} `dirname ${INSTALL_DIR}`
fi

if [[ ! -e /usr/bin/dotfiles ]] || [[ ${DOTFILES_BIN} -nt /usr/bin/dotfiles ]]; then
    sudo cp -u ${DOTFILES_BIN} /usr/bin/dotfiles
fi

if [[ ! -x /usr/bin/dotfiles ]]; then
    sudo chmod +x /usr/bin/dotfiles
fi

