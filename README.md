My dotfiles, managed by dotfiles.sh wrapper script around gnu stow.   
Simple and efficient centralized and decentralized dotfiles configuration management.

```
This tool manages 'awesome' & improved user dotfiles from a single distributed source.

Run 'dotfiles --init' to use them, amongst its improvements are:
- Syntax highlighting / checking and powerline for vim.
- Handy bash aliases and git enhancements.
- Customized shell prompt with pretty colors.
- 'Smart' bash completion.
- Optional capability: Set custom variables in ~/.extend_bashrc, these will overwrite vars from dotfiles.
- Optional capability: Define a git url in ${DOTFILE_RC} to use your own git repo as source.
  > This file should include a string like: GIT_URL='https://gitlab.com/{User_name}/{Repo_name}.git'

Usage:
dotfiles --init
    Install awesome dotfiles for bash, vim and git from ${SOURCE_DIR} or a git repo defined in ${DOTFILE_RC}.

dotfiles --upgrade
    Upgrade existing awesome dotfiles, syncing latest modifications from ${SOURCE_DIR} or ${DOTFILE_RC}.

dotfiles --restore
    Restoring user dotfiles from ${BACKUP_DIR}, overwriting awesome dotfiles in the process.

dotfiles --disable
    Purge awesome dotfiles and restore backup dotfiles from directory: ${BACKUP_DIR}.

 - Note:
   Within the ~/.dotfilerc file, you can set specific variables to adjust the tool's behaviour to your own preferences:
    SOURCE_DIR=\"~/.source_dotfiles\"   # Set the managed dotfiles directory, defaults to /opt/dotfiles
    DEST_DIR=\"~/.my_dotfiles\"         # Set your own dotfiles directory, defaults to ~/.dotfiles
    BACKUP_DIR=\"~/.my_backups\"        # Set the backup directory, defaults to ~/.backup_dotfiles
    GIT_URL=\"https://git_url.git\"     # Optional: Set a git url to fetch dotfile data from, overrides SOURCE_DIR
    PRESERVE_PROFILE_VARS=\"true\"      # Default is true, set to false to stop preserving preset bash variables from ~/.bash_profile
    PRESERVE_GIT_USER=\"true\"          # Default is true, set to false to stop preserving user specific git config from ~/.gitconfig
```

[PayPal.Me](https://paypal.me/jnick85?locale.x=nl_NL)
